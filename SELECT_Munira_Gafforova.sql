--1
WITH StoreRevenue AS (
    SELECT
        s.store_id,
        p.staff_id,
        SUM(p.amount) AS total_revenue
    FROM
        payment p
    INNER JOIN
        staff s ON p.staff_id = s.staff_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY
        s.store_id, p.staff_id
)
SELECT DISTINCT ON (store_id)
    store_id,
    staff_id,
    total_revenue
FROM
    StoreRevenue
ORDER BY
    store_id, total_revenue DESC;
--2
SELECT film.title, COUNT(rental.rental_id) AS rental_count
FROM film
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
GROUP BY film.title
ORDER BY rental_count DESC
LIMIT 5;
--3
SELECT actor.actor_id , first_name, last_name,
       MAX(release_year) - MIN(release_year) AS years_inactive
FROM actor
JOIN film_actor ON actor.actor_id = film_actor.actor_id
JOIN film ON film_actor.film_id = film.film_id
GROUP BY actor.actor_id, first_name, last_name
ORDER BY years_inactive DESC;